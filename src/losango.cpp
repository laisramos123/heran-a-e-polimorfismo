#include "losango.hpp"

float  Losango::get_distanciaMaior(){
        return distanciaMaior;
}
void Losango::set_distanciaMaior(float distanciaMaior){
        this->distanciaMaior = distanciaMaior;
}

float Losango::get_distanciaMenor(){
        return distanciaMenor;
}
void Losango::set_distanciaMenor(float distanciaMenor){
        this->distanciaMenor = distanciaMenor;
}


Losango::Losango(){
    set_tipo("Losango");
    set_distanciaMenor(5.0);
    set_distanciaMaior(8.0);
}

Losango::Losango(float distanciaMenor,float distanciaMaior){
    set_tipo("Losango");
    set_distanciaMenor(distanciaMenor);
    set_distanciaMaior(distanciaMaior);
}

Losango::~Losango(){
 cout << "Destruindo o objeto: " << get_tipo() << endl;
}

 

float Losango::calcula_area(){
        return (get_distanciaMenor()*get_distanciaMaior())/2;
}
float Losango::calcula_perimetro(){
        return sqrt((powf(get_distanciaMenor(),2) + powf(get_distanciaMaior(),2) )*4) ;
}