#ifndef LOSANGO_HPP_
#define LOSANGO_HPP_

#include "formageometrica.hpp"

class Losango: public FormaGeometrica{
private:
    float distanciaMaior;
    float distanciaMenor;

public:
    Losango();
    Losango(float distanciaMenor,float distanciaMaior);
    ~Losango();
    float get_distanciaMaior();
    void set_distanciaMaior(float distanciaMaior);
    float get_distanciaMenor();
    void set_distanciaMenor(float distanciaMenor);
    float calcula_area();
    float calcula_perimetro();
     
};

 


#endif